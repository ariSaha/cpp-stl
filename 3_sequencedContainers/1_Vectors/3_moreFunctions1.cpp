// insert
#include <iostream>
#include <vector>
using namespace std;

int main() {
    vector<int> v{7, 10, 18, 1, 13};
    
    auto i = v.insert(v.begin() + 2, 80);
    v.insert(v.begin() + 1, 2, 200);

    vector<int> v1;
    v1.insert(v1.begin(), v.begin(), v.begin() + 2); // (v.begin() + 2) is not included;

    for(int x : v) {
        cout << x << " ";
    }

    cout << "\n";
    
    for(int x : v1) {
        cout << x << " ";
    }
}