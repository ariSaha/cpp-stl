// pop_back, front, back
#include <iostream>
#include <vector>
using namespace std;

int main() {
    vector<int> v{7, 10, 18, 1, 13};
    
    v.pop_back();

    v.front() = 6;

    cout << v.front() << " " << v.back() << endl;
}