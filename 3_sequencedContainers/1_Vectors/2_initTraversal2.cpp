#include <iostream>
#include <vector>
using namespace std;

int main () {
    vector<int> v{5, 9, 12, 7, 2};

    for (auto i = v.begin(); i != v.end(); i++) {
        cout << *i << " ";
    }
}