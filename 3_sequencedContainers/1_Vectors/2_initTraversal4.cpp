#include <iostream>
#include <vector>
using namespace std;

int main () {
    int arr[] = {3, 7, 9, 2, 5};
    int n = sizeof(arr)/sizeof(arr[0]);

    vector<int> v(arr, arr + n);

    for (auto i = v.rbegin(); i != v.rend(); i++) {
        cout << *i << " ";
    }
}