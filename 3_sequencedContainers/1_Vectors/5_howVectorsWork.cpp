#include <iostream>
#include <vector>
using namespace std;

int main() {
    vector<int> v;
    cout << v.size() << " " << v.capacity() << endl;
    v.push_back(10);
    cout << v.size() << " " << v.capacity() << endl;
    v.push_back(15);
    cout << v.size() << " " << v.capacity() << endl;
    v.push_back(20);
    cout << v.size() << " " << v.capacity() << endl;
    v.push_back(25);
    cout << v.size() << " " << v.capacity() << endl;
    v.push_back(30);
    cout << v.size() << " " << v.capacity() << endl;
}

// Vector gets doubled in size - copies the element from prev. vector - deallocates memory for prev. vector
// Average Time Complexity - O(1) - but exactly it takes O(n) for this operation (insertion when capacity = size)