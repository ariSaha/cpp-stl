// erase
#include <iostream>
#include <vector>
using namespace std;

int main() {
    vector<int> v{7, 10, 18, 1, 13};
    
    v.erase(v.begin());

    for(int x : v) {
        cout << x << " ";
    }

    cout << "\n";

    v.erase(v.begin(), v.end() - 1); // (v.end() - 1) is not included;
    
    for(int x : v) {
        cout << x << " ";
    }
}