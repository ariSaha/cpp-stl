// resize
#include <iostream>
#include <vector>
using namespace std;

int main() {
    vector<int> v{7, 10, 18, 1};
    
    v.resize(3);
    for(int x : v) {
        cout << x << " ";
    }
    cout << "\n";

    v.resize(6);
    for(int x : v) {
        cout << x << " ";
    }
    cout << "\n";

    v.resize(8, 100); // defaultvalue = 100
    for(int x : v) {
        cout << x << " ";
    }
    cout << "\n";
}

// Think about the Time Complexities of the functions!