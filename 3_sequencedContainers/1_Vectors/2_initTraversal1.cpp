#include <iostream>
#include <vector>
using namespace std;

int main () {
    vector<int> v{5, 9, 12, 7, 2};

    // for (int i : v) {
    //     i = 5;
    // }

    for (int i : v) {
        cout << i << " ";
    }
}