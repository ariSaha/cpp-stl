// clear, empty
#include <iostream>
#include <vector>
using namespace std;

int main() {
    vector<int> v{7, 10, 18, 1, 13};
    
    v.clear();

    v.empty() ? cout << "Empty"  << endl : cout << "Not Empty" << endl;

}