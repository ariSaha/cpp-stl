#include <iostream>
#include <vector>
using namespace std;

void fun(vector<int> &v) { // passing by reference - for efficiency reasons (to avoid local copying of whole vector!)
    v.push_back(13);
    v.push_back(8);
}

int main() {
    vector<int> v{7, 10, 18, 1};
    fun(v);
    for(const int &x : v) { // referenced to avoid copying of element - useful with user-defined DataTypes - Efficiency++ :P
        cout << x << " "; // use const if we don't want to change the element
    }
}