// template Function
#include<iostream>
using namespace std;

template <typename T/*, int limit*/>

T maxArr(T arr[], int n) {
    // if (limit < n) {
    //      some error handling
    // }
    T res = arr[0];
    for (int i = 0; i < n; ++i) {
        if (arr[i] > res) {
            res =  arr[i];
        }
    }
    return res;
}

int main() {
    int arrOne[] = {3, 9, 2, 7, 10};
    float arrTwo[] = {6.9, 7.8, 12.7, 5.1, 8.1, 1.5};
    cout << maxArr<int>(arrOne, 5) << "\n";
    cout << maxArr<float/*, 10*/>(arrTwo, 6) << "\n";
}
