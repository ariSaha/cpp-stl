#include<iostream>
using namespace std;

template <typename T>
struct Pair {
    T x, y;
    Pair (T i, T j) {
        x = i;
        y = j;
    }
    T getFirst();
    T getSecond() {
        return y;
    }
};

template <typename T>
T Pair<T>::getFirst() {
    return x;
}

int main() {
    Pair<int> P(10, 20);
    cout << P.getFirst() << "\n";
    cout << P.getSecond() << "\n";
}
