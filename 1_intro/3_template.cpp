// template Function
#include<iostream>
using namespace std;

template <typename T>

T maxm(T x, T y) {
    return (x > y) ? x : y;
}

int main() {
    cout << maxm<int>(5, 7) << "\n";
    cout << maxm<char>('c', 'g') << "\n";
}
