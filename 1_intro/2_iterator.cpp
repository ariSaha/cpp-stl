// Gives Address of element in container
// start(), end()
#include<iostream>
#include<vector>
using namespace std;

int main() {

	vector<int> v = {10, 20, 30, 40, 50};
    // vector<int>::iterator i = v.begin();
    auto i = v.begin();
    cout << *i << "\n";
    i++;
    cout << *i << "\n";
    i = v.end();
    i--;
    cout << *i << "\n";
    return 0;
}
