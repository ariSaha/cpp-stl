#include <iostream>
#include <utility>
#include <algorithm>
using namespace std;

void sortArr(int a[], char b[], int n) {

    pair<int, char> p;
    pair<int, char> arr[n];
    for (int i = 0; i < n; i++) {
        p = {a[i], b[i]};
        arr[i] = p;
    }

    sort(arr, arr + n);

    for (int i = 0; i < n; i++) {
        b[i] = arr[i].second;
    }

    for (int i = 0; i < n; i++) {
        cout << b[i] << " ";
    }
}

int main() {
    int a[] = {10, 15, 5};
    char b[] = {'X', 'Y', 'Z'};
    sortArr(a, b, 3);
}