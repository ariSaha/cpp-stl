#include <iostream>
// #include <utility>
using namespace std;

int main() {
    pair<int, int> p1;
    cout << p1.first << " " << p1.second << endl;
    p1 = {10, 20};
    cout << p1.first << " " << p1.second << endl;

    pair<int, string> p2(1, "ari");
    cout << p2.first << " " << p2.second << endl;
}