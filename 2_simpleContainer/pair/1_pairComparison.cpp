#include <iostream>
#include <utility>
using namespace std;

int main() {
    pair<int, int> p1(1, 10), p2(7, 3);
    cout << (p1 == p2) << endl;
    cout << (p1 != p2) << endl;
    cout << (p1 > p2) << endl;
    cout << (p1 < p2) << endl;
}